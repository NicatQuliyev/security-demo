package com.ingress.securitydemo.service;

import com.ingress.securitydemo.dto.SignUpRequest;
import com.ingress.securitydemo.model.Authority;
import com.ingress.securitydemo.model.User;
import com.ingress.securitydemo.repository.AuthorityRepository;
import com.ingress.securitydemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    private final EmailService emailService;

    private final AuthorityRepository authorityRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    public ResponseEntity registerUser(SignUpRequest signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity("Username is already taken!",
                    HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity("Email Address already in use!",
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getPassword(), signUpRequest.getUsername(), signUpRequest.getEmail());


        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        if (signUpRequest.getAuthorityId() == null) {
            signUpRequest.setAuthorityId(1L);
        }

        Authority userAuthority = authorityRepository.findById(signUpRequest.getAuthorityId())
                .orElseThrow(() -> new RuntimeException("User authority not set!"));

        user.setAuthorities(Collections.singletonList(userAuthority));

        String confirmationToken = getConfirmationToken();

        user.setConfirmationToken(confirmationToken);
        userRepository.save(user);

        emailService.sendMail(signUpRequest.getEmail(),
                "Confirmation",
                "http://localhost:9090/api/auth/confirmation?confirmationToken=" + confirmationToken);


        return ResponseEntity.ok("User registered successfully");

    }

    public ResponseEntity<?> confirmation(String confirmationToken) {
        Optional<User> user = userRepository.findByConfirmationToken(confirmationToken);
        if (user.isPresent()) {
            User user1 = user.get();
            userRepository.save(user1);

            return ResponseEntity.ok("User confirmed successfully");
        } else {
            return ResponseEntity.ok("Confirmation token is invalid");
        }


    }

    private String getConfirmationToken() {
        UUID gfg = UUID.randomUUID();
        return gfg.toString();
    }
}
