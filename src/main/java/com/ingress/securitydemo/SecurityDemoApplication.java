package com.ingress.securitydemo;

import com.ingress.securitydemo.model.User;
import com.ingress.securitydemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class SecurityDemoApplication implements CommandLineRunner {

//    private final UserRepository userRepository;
//    private final AuthorityRepository authorityRepository;
//    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SecurityDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        Authority user = Authority.builder()
//                .authority("USER")
//                .build();
//
//        Authority admin = Authority.builder()
//                .authority("ADMIN")
//                .build();
//
//        authorityRepository.save(admin);
//        authorityRepository.save(user);
//
//
//        User user1 = User.builder()
//                .username("user")
//                .password(bCryptPasswordEncoder.encode("12345"))
//                .authorities(List.of(user))
//                .build();
//
//        User admin2 = User.builder()
//                .username("admin")
//                .password(bCryptPasswordEncoder.encode("12345"))
//                .authorities(List.of(admin))
//                .build();
//
//        userRepository.save(user1);
//        userRepository.save(admin2);
//    }
    }
}
